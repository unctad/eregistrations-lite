# in /compose/.env
* replace all image tags with the newest desired images
* replace other environment variable value placeholders with the proper values

# in /compose/docker-compose.yml
* replace all occurrences of YOUR_DOMAIN_NAME with proper value

# in /haproxy/haproxy.cfg 
* replace all occurrences of YOUR_DOMAIN_NAME with proper value

# in /haproxy/bpa-server-whitelist.lst
* add IP of the server that is hosting BPA. That server must have permission to do PUT and POST requests into the formio and restheart containers. Those permissions are given in haproxy.cfg file.  
