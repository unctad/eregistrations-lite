global
        log /dev/log    local0
        log /dev/log    local1 notice
        chroot /var/lib/haproxy
        stats socket /run/haproxy/admin.sock mode 660 level admin
        stats timeout 90s
        user haproxy
        group haproxy
        daemon
        maxconn 2048

        # Default SSL material locations
        ca-base /etc/ssl/certs
        #crt-base /etc/ssl/private
        crt-base /etc/letsencrypt/live/YOUR_DOMAIN_NAME
        # Default ciphers to use on SSL-enabled listening sockets.
        # For more information, see ciphers(1SSL). This list is from:
        #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
        tune.ssl.default-dh-param 2048
        #ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS
        ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS
        ssl-default-bind-options no-sslv3

defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        option  forwardfor
        timeout connect 5000
        timeout client  150000
        timeout server  150000
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

userlist UsersFor_kibana
        user kibana password $6$ounv5tqc$QLs4U9rymn2PayBzdSq/2GxvLlgmMb21fZgXQRuxPZI/DSh1b9gewHY0tyrVBYWwbXUqeCkMQrr/h4KuxDowX.

frontend www_80
    mode http
    bind *:80
    acl host_display hdr_beg(host) -i YOUR_DOMAIN_NAME
    acl host_partc hdr_beg(host) -i partc.YOUR_DOMAIN_NAME
    acl host_cas hdr_beg(host) -i eid.YOUR_DOMAIN_NAME
    acl host_graylog hdr_beg(host) -i graylog.YOUR_DOMAIN_NAME
    acl host_gdbs hdr_beg(host) -i gdbs.YOUR_DOMAIN_NAME
    redirect location https://YOUR_DOMAIN_NAME if host_display
    redirect location https://partc.YOUR_DOMAIN_NAME if host_partc
    redirect location https://eid.YOUR_DOMAIN_NAME if host_cas
    redirect location https://graylog.YOUR_DOMAIN_NAME if host_graylog
    redirect location https://gdbs.YOUR_DOMAIN_NAME if host_gdbs


frontend www-https
   mode http
   option httplog
   bind *:443 ssl crt /etc/letsencrypt/live/YOUR_DOMAIN_NAME/haproxy.crt
   compression algo gzip
   compression type text/css text/html text/javascript application/javascript text/plain text/xml application/json image/svg+xml
   reqadd X-Forwarded-Proto:\ https
   acl is_camunda path_beg /camunda/
   acl is_restheart path_beg /restheart/
   acl activemq url_beg /activemq/
   acl kibana_path path_beg -i /analytics
   acl kibana_cookie hdr_sub(cookie) kibana_access=f282722936e96234a7c68fddeb202687
   acl partc_path url_beg /partc
   acl cas_path url_beg /cback
   acl formio_path url_beg /formio
   acl is_cms hdr(Host) -i YOUR_DOMAIN_NAME
   acl is_cas hdr(Host) -i eid.YOUR_DOMAIN_NAME
   acl is_partc hdr(Host) -i partc.YOUR_DOMAIN_NAME

   use_backend activemq_backend if activemq
   use_backend formio if formio_path
   use_backend camunda_backends if is_camunda
   use_backend restheart_backends if is_restheart
   use_backend back_kibana if kibana_path kibana_cookie
   use_backend cas_backend if is_cas cas_path
   use_backend partc_backend if partc_path
   use_backend partc_frontend if is_partc
   use_backend cas_frontend if is_cas
   use_backend cms if  is_cms
   use_backend graylog_backends if { hdr(Host) -i graylog.YOUR_DOMAIN_NAME   }
   use_backend general_database_backends if { hdr(Host) -i gdbs.YOUR_DOMAIN_NAME   }

   capture request header origin len 120
   rspadd Access-Control-Allow-Headers:\ Accept,\ Accept-Encoding,\ Authorization,\ Content-Length,\ Content-Type,\ Host,\ If-Match,\ If-None-Match,\ Origin,\ X-Requested-With,\ User-Agent,\ No-Auth-Challenge,\ Pragma,\ X-JWT-Token,\ Cache-Control if { capture.req.hdr(0) -m found }

backend partc_frontend
   mode http
   server partc_frontend 127.0.0.1:4400

backend cas_frontend
   mode http
   server cas_frontend 127.0.0.1:4401

backend partc_backend
  mode http
  server partc_backend 127.0.0.1:8383

backend cas_backend
  mode http
  acl eregulations_admin_token path_end /getEregulationsAdminToken
  acl partc_token path_end /getPartCToken
  acl camunda_token path_end /getCamundaToken
  acl cms_token path_end /getCmsToken
  http-request deny if eregulations_admin_token
  http-request deny if partc_token
  http-request deny if camunda_token
  http-request deny if cms_token
  server cas_backend 127.0.0.1:8282

backend cms
  mode http
  server cms 127.0.0.1:6020

backend back_kibana
  mode http
  reqrep ^([^\ ]*)\ /analytics[/]?(.*) \1\ /\2
  server kibana 127.0.0.1:5601

backend formio
  mode http
  acl public_valid_method method GET OPTIONS
  acl private_valid_method method POST PUT
  acl whitelist src -f /etc/haproxy/bpa-server-whitelist.lst
  http-request deny if private_valid_method !whitelist
  http-request deny if !public_valid_method !private_valid_method
  reqrep ^([^\ ]*)\ /formio/(.*) \1\ /\2
  http-response set-header Content-Type application/json
  server formio 127.0.0.1:3001

backend restheart_backends
  mode http
  acl public_valid_method method GET OPTIONS
  acl private_valid_method method POST PUT
  acl whitelist src -f /etc/haproxy/bpa-server-whitelist.lst
  http-request deny if private_valid_method !whitelist
  http-request deny if !public_valid_method !private_valid_method
  reqadd Authorization:\ Basic\ YWRtaW46ZXJlZzEyMw==
  http-response set-header Content-Type application/json
  server restheart 127.0.0.1:9080

backend camunda_backends
  mode http
  reqrep ^([^\ ]*)\ /camunda/(.*) \1\ /\2
  server camunda 127.0.0.1:6009

backend graylog_backends
  mode http
  server graylog 127.0.0.1:9005

backend general_database_backends
  mode http
  server general_database 127.0.0.1:3003

backend activemq_backend
  mode http
  acl whitelist src -f /etc/haproxy/bpa-server-whitelist.lst
  http-request deny if !whitelist
  reqrep ^([^\ ]*)\ /activemq/(.*) \1\ /\2
  server activemq 127.0.0.1:61616
